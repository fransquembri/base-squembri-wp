<?php get_header(); ?>
<section class="error-404">
	<div class="container cont-1200">
		<h1>Error 404 - Página no encontrada.</h1>
		<p>¡Vaya! Parece que la página que buscas no existe :( </p>
		<p class="mas">
			<a href="<?php echo get_home_url(); ?>">Volver a la home</a>
		</p>
	</div>
</section>
<?php get_footer(); ?>