// variables para llamar a los módulos
var gulp = require('gulp')
var postcss = require('gulp-postcss')
var atImport = require("postcss-import")
var precss = require("precss")
var cssnext = require('postcss-cssnext')
// var cssnano = require("cssnano")
var assets = require("postcss-assets")


// Creamos la tarea para leer el archivo fuente CSS y procesarlos con PostCSS en la carpeta destino 'assets/css'
gulp.task('css', function () {
 var processors = [
 atImport,
 precss,
 cssnext,
 // cssnano,
 assets
 ]
 return gulp.src('./assets/src/style.css')
 .pipe(postcss(processors))
 .pipe(postcss([assets({
      loadPaths: ['./assets/images/']
    })]))
 .pipe(gulp.dest('./assets/css'))
})

// Watch
gulp.task('watch', function () {
 gulp.watch('assets/src/**/*.css', ['css'])
})

// Default
gulp.task('default', ['css', 'watch'])