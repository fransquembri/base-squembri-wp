<?php
 
  if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Please do not load this page directly. Thanks!');
 
  if ( post_password_required() ) { ?>
    This post is password protected. Enter the password to view comments.
  <?php
    return;
  }
?>
 
<?php if ( have_comments() ) : ?>
 
   
 
  <div class="navigation">
    <div class="next-posts"><?php previous_comments_link() ?></div>
    <div class="prev-posts"><?php next_comments_link() ?></div>
  </div>
 
  <div class="mod-comments">
    <h2 id="comments"><?php comments_number('No hay comentarios', 'Un comentario', '% Comentarios' );?></h2>
    <section class="commentlist">
      <?php wp_list_comments('type=comment&callback=pew_comments'); ?>
    </section>
  </div>
   
 
  <div class="navigation">
    <div class="next-posts"><?php previous_comments_link() ?></div>
    <div class="prev-posts"><?php next_comments_link() ?></div>
  </div>
 
 <?php else : // this is displayed if there are no comments so far ?>
 
  <?php if ( comments_open() ) : ?>
    <!-- If comments are open, but there are no comments. -->
 
   <?php else : // comments are closed ?>
    <p>Comments are closed.</p>
 
  <?php endif; ?>
 
<?php endif; ?>
 
<?php if ( comments_open() ) : ?>
 
<div id="respond">
 
  <h2><?php comment_form_title( 'Déjanos tu comentario', 'Deja tu comentario para %s' ); ?></h2>
 
  <div class="cancel-comment-reply">
    <?php cancel_comment_reply_link(); ?>
  </div>
 
  <?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
    <p>You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>">logged in</a> to post a comment.</p>
  <?php else : ?>
 
  <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
 
    <?php if ( is_user_logged_in() ) : ?>
 
      <p>Logueado como <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Desloguearse »</a></p>
 
    <?php else : ?>
 
      <div class="col-md-6 pl0">
        <input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> placeholder="Nombre" />        
      </div>
 
      <div class="col-md-6 pr0">
        <input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> placeholder="E-mail" />
      </div>
 
    <?php endif; ?>
 
    <!--<p>You can use these tags: <code><?php echo allowed_tags(); ?></code></p>-->
 
    <div>
      <textarea name="comment" id="comment" cols="58" rows="10" tabindex="4" placeholder="Tu comentario"></textarea>
    </div>
 
    <div class="politica-submit">
      <div class="col-md-6 pl0">
        <label for="check">
          <input type="checkbox" id="check" required class="grey">
          <span>He leído y acepto la <a rel="nofollow" onclick="window.open(this.href, this.target, 'width=400,height=720, scrollbars=1 '); return false;" target="_blank" href="<?php echo get_home_url(); ?>/politica-de-privacidad">política de privacidad</a></span>
        </label>     
      </div>
 
      <div class="col-md-6 pr0">
        <input name="submit" type="submit" id="submit" class="btn btn-sand right" tabindex="5" value="<?php _e('Añadir comentario', 'theme ysf'); ?>" />
      </div>
      
      <?php comment_id_fields(); ?>
    </div>
 
    <?php do_action('comment_form', $post->ID); ?>
 
  </form>
 
  <?php endif; // If registration required and not logged in ?>
 
</div>
 
<?php endif; ?>