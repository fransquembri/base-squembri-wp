<!-- mapa -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzALZ9EHaHBzzkz4tVhZrNAe9ZD6hKmN0"></script>
    <script type="text/javascript">
    function initialize() {
      var marcadores = [           
        [
          //LAT = 0
         37.1512287,  
 
        //LNG = 1
        -3.6254696, 
         
        // NOMBRE
        'Escuela Internacional de cortadores de Jamón',

        // TELEFONO malaga = 4
        'Geolit, Parque Científico y Tecnológico <br> Calle Sierra Morena s/n, 23620, Jaén',

        // MOVIL malaga = 5
        '953 96 14 14',

        // EMAIL malaga = 6
        'www.escueladeljamon.com',
 
        ],
        [
          //LAT = 0
         40.4155691,  
 
        //LNG = 1
        -3.7018227, 
         
        // NOMBRE
        'Escuela Internacional de cortadores 2 de Jamón',

        // TELEFONO malaga = 4
        'Geolit, Parque Científico y Tecnológico <br> Calle Sierra Morena s/n, 23620, Jaén',

        // MOVIL malaga = 5
        '953 96 14 14',

        // EMAIL malaga = 6
        'www.escueladeljamon.com',
 
        ],
        [
          //LAT = 0
         42.4155691,  
 
        //LNG = 1
        -3.8018227, 
         
        // NOMBRE
        'Escuela Internacional de cortadores 3 de Jamón',

        // TELEFONO malaga = 4
        'Geolit, Parque Científico y Tecnológico <br> Calle Sierra Morena s/n, 23620, Jaén',

        // MOVIL malaga = 5
        '953 96 14 14',

        // EMAIL malaga = 6
        'www.escueladeljamon.com',
 
        ],
      ];
      var map = new google.maps.Map(document.getElementById('mapa'), {
        // Zoom que se le aplica al mapa
        zoom: 14,
        scrollwheel:  false,
        // Coordenadas LatLng de google 
        center: new google.maps.LatLng(40.4167094,-3.703946), // Andalucía
        //Tipo de mapa
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // Estilos y colores del mapa 
        styles: [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"water","stylers":[{"color":"#84afa3"},{"lightness":52}]},{"stylers":[{"saturation":-77}]},{"featureType":"road"}]
      });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;
         
      /* Mapa con menos zoom en mobile */
      var width_page = $(document).width();  
      if( width_page > 767 ) {
        map.setZoom(6.7);
      }
      else {
        map.setZoom(5);
      }
      /* Fin zoom Mobile */
 
      for (i = 0; i < marcadores.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(marcadores[i][0], marcadores[i][1]),
          map: map,
          title: marcadores[i][2],
          //icono de marcador 
          // En caso de querer introducir un marcador propio unico introducir la url entre las ''
          icon: '<?php echo get_template_directory_uri(); ?>/library/images/marcador.png',
          //Función que llama a la animación de los marcadores 
          // Sin este código no se animan los marcadores
          animation: google.maps.Animation.DROP
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          //bucle que recoje toda la información de la lista marcadores y la introduce en codigo html y css

            return function() {
              infowindow.setContent(// Estilo de la caja de texto 
                  "<div class='content-map' style='width:260px;min-height:60px'><h3>"
                  + marcadores[i][2] //NOMBRE DEL ESTUDIO
                  + "</h3>"
                  + "<p class='dir'>"
                  + marcadores[i][3] //DIRECCIÓN 1 DEL ESTUDIO
                  + "</p>"
                  + "<p class='tel'><a href='tel:0034" + marcadores[i][4] + "' >T + (34) "
                  + marcadores[i][4] //DIRECCIÓN 1 DEL ESTUDIO
                  + "</a></p>"
                  + "<p class='link' ><a href='"
                  + marcadores[i][5] // TELEFONO DEL ESTUDIO
                  + "' target='_blank'>" + marcadores[i][5] + "</a></p>"
                  )
                ;
              infowindow.open(map, marker);
            }
        })(marker, i));
      }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>