# Framework Wordpress #

### Versión 1.1 ###

* Implementación de title_excerpt();
  * Se usa igual que excerpt(), se pone el número de caracteres entre parentesis para filtrar la longitud del título.
  
* Implementación de sanitize_name
  * Esta función se utiliza para limpiar de acentos, espacios y caracteres especiales los archivos que se suben a medios. 
  * Se implementa en library/tools/sanitize-filename.php
  * Nombre de archivo sanitize-filename.php