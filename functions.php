<?php
/*
URL: http://www.squembri.com
*/

// Get Pew
require_once('assets/pew.php'); // custom theme functions

// Admin Functions (commented out by default)
require_once('assets/back.php'); // custom admin functions

// Custom Post Type Theme
require_once('assets/custom-post-type.php'); // custom admin functions

/*********************
LAUNCH pew
Let's get everything up and running.
*********************/
function pew_ahoy() {

  // Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // launching operation cleanup
  add_action( 'init', 'pew_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'pew_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'pew_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'pew_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'pew_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'pew_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  pew_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'pew_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'pew_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'pew_excerpt_more' );

} /* end pew ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'pew_ahoy' );


/************* EMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
  $content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'post-single', 1600, 550, true );
add_image_size( 'post-blog', 1000, 400, true );
add_image_size( 'post-home', 767, 767, true );


add_filter( 'image_size_names_choose', 'pew_custom_image_sizes' );

function pew_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'pew-thumb-600' => __('600px por 150px'),
        'pew-thumb-300' => __('300px por 100px'),
    ) );
}
/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function pew_register_sidebars() {
  register_sidebar(array(
    'id' => 'sidebar1',
    'name' => __( 'Sidebar', 'pew' ),
    'description' => __( 'Arrastra aquí los elementos de tu sidebar.', 'pew' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  /*
  to add more sidebars or widgetized areas, just copy
  and edit the above sidebar code. In order to call
  your new sidebar just use the following code:

  Just change the name to whatever your new
  sidebar's id is, for example:

  register_sidebar(array(
    'id' => 'sidebar2',
    'name' => __( 'Sidebar 2', 'pew' ),
    'description' => __( 'The second (secondary) sidebar.', 'pew' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  To call the sidebar in your template, you can just copy
  the sidebar.php file and rename it to your sidebar's name.
  So using the above example, it would be:
  sidebar-sidebar2.php

  */
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function pew_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('listado-comentarios'); ?>>
    <article class="item-comment">
        <?php // end custom gravatar call ?>
        <?php printf(__( '<div class="name-date"><h5 class="name-comment">%1$s</h5>', 'risbox' ), get_comment_author_link(), edit_comment_link(__( 'Editar', 'risbox' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('j-m-Y'); ?>"><?php echo comment_time('M d, Y'); ?></time></div>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'risbox' ) ?></p>
        </div>
      <?php endif; ?>
      <div class="comment-content">
        <?php comment_text() ?>
      </div>
      <div class="comment-resp">
        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      </div>  
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!


// Remove Open Sans that WP adds from frontend
if (!function_exists('remove_wp_open_sans')) :
    function remove_wp_open_sans() {
        wp_deregister_style( 'open-sans' );
        wp_register_style( 'open-sans', false );
    }
    add_action('wp_enqueue_scripts', 'remove_wp_open_sans');

    // Uncomment below to remove from admin
    // add_action('admin_enqueue_scripts', 'remove_wp_open_sans');
endif;

remove_action( 'wp_head', 'rest_output_link_wp_head');


// datos propios de la empresa
if ( file_exists( TEMPLATEPATH . '/class.my-theme-options.php' ) ) {
  require_once( TEMPLATEPATH . '/class.my-theme-options.php' );
}

add_action( 'admin_bar_menu', 'modificar_datos_empresa', 999 );

function modificar_datos_empresa( $wp_admin_bar ) {
  $sitio = get_home_url().'/wp-admin/themes.php?page=mytheme-options';
  $nombre = get_bloginfo( 'name' );
  $args = array(
    'id'    => 'my_page',
    'title' => 'Modificar Datos de '.$nombre,
    'href'  => $sitio,
    'meta'  => array( 'class' => 'my-toolbar-page' )
  );
  $wp_admin_bar->add_node( $args );
}

